-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2017 at 06:56 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`name`, `email`, `password`) VALUES
('admin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `c_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `address` text,
  `phone` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `c_name`, `email`, `password`, `address`, `phone`) VALUES
(3, 'client', 'client@gmail.com', '62608e08adc29a8d6dbc9754e659f125', 'Dhanmondi', '017XXXXXXXX'),
(2, 'Nasir', 'nasir@gmail.com', '78e96b7de2cfaa6d3743781169c32680', 'Dhaka, Dhanmondi', '01763-433486'),
(1, 'raihan', 'raihan@gmail.com', 'raihan', 'dhanmondi', '017XXXXXXXX'),
(4, 'uiu', 'uiu@gmail.com', '1a84f397e02ac24a39566fe1095a81ff', 'uiu', '0176562365');

-- --------------------------------------------------------

--
-- Table structure for table `disctrict`
--

CREATE TABLE `disctrict` (
  `id` int(11) NOT NULL,
  `dist_name` varchar(50) NOT NULL,
  `div_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disctrict`
--

INSERT INTO `disctrict` (`id`, `dist_name`, `div_id`) VALUES
(14, 'Bandarban', 6),
(8, 'Brahmanbaria', 6),
(7, 'Chadpur', 6),
(13, 'Chittagong', 6),
(1, 'Comilla', 6),
(2, 'Cox\'s Bazar', 6),
(12, 'Dhaka', 1),
(11, 'Dinajpur', 5),
(10, 'Gaibanda', 2),
(6, 'Gopal gonj', 1),
(5, 'Munsigonj', 1),
(9, 'Mymanshing', 1),
(4, 'Narsingdi', 1),
(3, 'Rangamati', 6);

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `div_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `div_name`) VALUES
(1, 'Dhaka'),
(2, 'Rajshahi'),
(3, 'Sylhet'),
(4, 'Barishal'),
(5, 'Rangpur'),
(6, 'Chittagong'),
(7, 'Khulna');

-- --------------------------------------------------------

--
-- Table structure for table `heritage`
--

CREATE TABLE `heritage` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `div_id` int(11) DEFAULT NULL,
  `dist_id` int(11) DEFAULT NULL,
  `address` text,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `heritage`
--

INSERT INTO `heritage` (`id`, `name`, `div_id`, `dist_id`, `address`, `type`) VALUES
(7, 'Boga Lake', 6, 14, 'BandarBan', 'Nature'),
(5, 'Khaoiyachora', 6, 3, 'Mirsorai, Chittagong ', 'Water Fall'),
(2, 'Moynamoti', 6, 1, 'Moynamoti, comilla', 'Archeology'),
(8, 'Naafakhum', 6, 14, 'Remakri, end of the Sangu River, Bandarban, Bangladesh', 'Water Fall'),
(4, 'National zoo', 1, 12, 'Mirpur', 'Wild life'),
(1, 'Sea Beach', 6, 2, 'Cox\'s bazar', 'Beach'),
(3, 'Shapnopur', 2, 10, 'Gaibanda', 'Park'),
(6, 'WaariBotessshor', 1, 4, 'Waari,Belabo, Narsingdi', 'Archaeological ');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `special_package` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `email`, `package_id`, `special_package`) VALUES
(1, 'client@gmail.com', 1, 0),
(2, 'client@gmail.com', 1, 0),
(3, 'client@gmail.com', 1, 0),
(4, 'client@gmail.com', 1, 0),
(5, 'client@gmail.com', 1, 0),
(6, 'client@gmail.com', 1, 0),
(7, 'client@gmail.com', 1, 0),
(8, 'client@gmail.com', 1, 0),
(9, 'client@gmail.com', 1, 0),
(10, 'client@gmail.com', 1, 0),
(11, 'client@gmail.com', 1, 0),
(12, 'client@gmail.com', 1, 0),
(13, 'client@gmail.com', 1, 0),
(14, 'client@gmail.com', 1, 0),
(15, 'client@gmail.com', 1, 0),
(16, 'client@gmail.com', 1, 0),
(17, 'client@gmail.com', 1, 0),
(18, 'client@gmail.com', 1, 0),
(19, 'client@gmail.com', 1, 0),
(20, 'client@gmail.com', 1, 0),
(21, 'client@gmail.com', 1, 0),
(22, 'client@gmail.com', 1, 0),
(23, 'client@gmail.com', 1, 0),
(24, 'client@gmail.com', 1, 0),
(25, 'client@gmail.com', 1, 0),
(26, 'client@gmail.com', 2, 0),
(30, 'uiu@gmail.com', 1, 0),
(31, 'uiu@gmail.com', 1, 0),
(32, 'client@gmail.com', 1, 0),
(33, 'client@gmail.com', 1, 0),
(35, 'client@gmail.com', 3, 0),
(36, 'client@gmail.com', 3, 0),
(37, 'client@gmail.com', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `from_dist` varchar(50) DEFAULT NULL,
  `to_heritage` varchar(200) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `tour_date` varchar(200) DEFAULT NULL,
  `dept_loc` varchar(200) DEFAULT NULL,
  `description` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `name`, `from_dist`, `to_heritage`, `price`, `days`, `tour_date`, `dept_loc`, `description`) VALUES
(1, 'Cox way', 'Comilla', 'Sea Beach', '3000.00', 5, '2017-08-17 00:00:00', 'Jangalia', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(2, 'Visiting Wild', 'Gaibanda', 'National zoo', '500.00', 1, '2017-08-31 06:30:00', 'Bogura bus stand', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(3, 'Boga Tour', 'Dhaka', 'Boga Lake', '6000.00', 7, '2017-09-03 06:36:05', 'Gabtoli', 'Bogakain Lake, also called Baga Lake or Boga Lake, is a lake located in Ruma Upazila in the hill district Bandarban, Bangladesh. It is a natural sweet and deep water lake. Its height from sea level is nearly 2,000 feet.\r\n'),
(4, 'Naafakhum', 'Narsingdi', 'Naafakhum', '7000.00', 4, '2017-09-02 23:00:00', 'Old Bus Stand', 'Nafa-khum is a waterfall in Bangladesh on the Sangu River. It is among the largest waterfalls in the country by volume of water falling. The wild hilly river Sangu suddenly falls down here about 25–30 feet. '),
(7, 'Boga', 'Comilla', 'Boga Lake', '20000.00', 3, '2017-09-21 00:00:00', 'Jhangalia', 'askfheiwsfxv'),
(13, 'Tour to moynamoti hosted by Web Dev', 'Comilla', 'Moynamoti', '5000.00', 5, '01:23:2014', 'safsfjk', 'asfewrwer'),
(14, 'asdfasdf', 'Chittagong', 'Naafakhum', '453453.00', 423, '2017-09-21', 'sdfsfasf', 'dsdfgdg');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `package_id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `comments` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `email`, `package_id`, `rating`, `comments`) VALUES
(1, 'nasir@gmail.com', 2, 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(2, 'raihan@gmail.com', 2, 5, ' a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'),
(3, 'client@gmail.com', 2, 3, 'sadfjkjgldgdglj'),
(4, 'client@gmail.com', 2, 2, 'weuiriwefkj'),
(5, 'client@gmail.com', 1, 3, 'asfsgdghfghgfj'),
(6, 'client@gmail.com', 1, 3, 'asfsgdghfghgfj');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`email`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `disctrict`
--
ALTER TABLE `disctrict`
  ADD PRIMARY KEY (`dist_name`),
  ADD KEY `div_id` (`div_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`div_name`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `heritage`
--
ALTER TABLE `heritage`
  ADD PRIMARY KEY (`name`),
  ADD KEY `dist_id` (`dist_id`),
  ADD KEY `div_id` (`div_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_dist` (`from_dist`),
  ADD KEY `to_heritage` (`to_heritage`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `package_id` (`package_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `disctrict`
--
ALTER TABLE `disctrict`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `heritage`
--
ALTER TABLE `heritage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `disctrict`
--
ALTER TABLE `disctrict`
  ADD CONSTRAINT `disctrict_ibfk_1` FOREIGN KEY (`div_id`) REFERENCES `division` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `heritage`
--
ALTER TABLE `heritage`
  ADD CONSTRAINT `heritage_ibfk_1` FOREIGN KEY (`dist_id`) REFERENCES `disctrict` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `heritage_ibfk_2` FOREIGN KEY (`div_id`) REFERENCES `division` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`email`) REFERENCES `client` (`email`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `package_ibfk_1` FOREIGN KEY (`from_dist`) REFERENCES `disctrict` (`dist_name`) ON DELETE SET NULL,
  ADD CONSTRAINT `package_ibfk_2` FOREIGN KEY (`to_heritage`) REFERENCES `heritage` (`name`) ON DELETE CASCADE;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`email`) REFERENCES `client` (`email`) ON DELETE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
